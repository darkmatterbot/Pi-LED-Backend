module.exports = {
  apps : [{
    name: "Pilight Webserver",
    script: 'pilight-WEB.js',
    args: "8888 22222",
    log_date_format: "YYYY-MM-DD HH:mm:ss"
  }, {
    name: "Pilight Led Server",
    script: './led_scripts/pilight-LED.py',
    interpreter: "/usr/bin/python3",
    args: "22222",
    log_date_format: "YYYY-MM-DD HH:mm:ss"
  }]
};
