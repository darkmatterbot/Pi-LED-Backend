'use strict'
// -- Dom Objects --

// OFF / DIMM BUTTONS
let off_button = document.getElementById("off");
let dimm_button = document.getElementById("dimm");
// RAINBOW BUTTON
let rainbow_button = document.getElementById("rainbow-button");
// SPEED SLIDER
let slider_container = document.querySelector(".slider-container");
let slider = document.getElementById("slider");
let output = document.getElementById("slider-output");
// COLOR INPUT
let color = document.getElementById("colorwheel");
let color_button = document.getElementById("colorwheel-button");
// CONNECTION STATUS BOX
let connection_status = document.getElementById("connection-status");

// all buttons in one place
let buttons = document.querySelectorAll("button");

// -- DEFAULT VALUES --
output.innerHTML = slider.value;
color_button.style.borderColor = color.value;

// register an event source for connection status
function connectionStatusHandler(event) {
    // server requested close or windows is closed/reloaded
    if(event.type === "close" || event.type === "error" || event.type === "beforeunload") {
        connectionStatusEventSource.close();
        return;
    }
    const connected = JSON.parse(event.data).is_connected;
    if(connected == true) {
        connection_status.className = "connected";
        connection_status.innerHTML = "LED control server online";
    }
    else {
        connection_status.className = "disconnected";
        connection_status.innerHTML = "LED control server offline";
    }
}
const connectionStatusEventSource = new EventSource("connection-status")
connectionStatusEventSource.addEventListener("connection-status", connectionStatusHandler);
connectionStatusEventSource.addEventListener("close", connectionStatusHandler);
// close stream on window reload/exit
window.addEventListener("beforeunload", connectionStatusHandler);
connectionStatusEventSource.addEventListener("error", connectionStatusHandler);



//#region -- Helper Functions --
function sendEffectRequest(effect, effectparam="") {
    const destination = "/effect";
    const data = new Object();
    data.effect = effect;
    data.effectparam = effectparam;
    const request = new XMLHttpRequest();
    request.open("POST", destination);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onreadystatechange = handler;
    function handler() {
        if(this.readyState === 4) {
            console.log(this.status)
            console.log(this.responseText)
        }
    }
    request.send(JSON.stringify(data));
}
function hideSpeedSlider() {
    // clear slider timeout if exists
    // clearTimeout(slider_container.slider_timeout_id);
    // hide rainbow speed slider when visible
    slider_container.classList.remove("visible");
}
//#endregion -- Helper Functions --


//  -- Event Handlers for all the buttons --
//#region

// OFF BUTTON
off_button.addEventListener("click", event => {
    hideSpeedSlider();
    sendEffectRequest("Off");
});

// DIMM BUTTON
dimm_button.addEventListener("click", event => {
    hideSpeedSlider();
    sendEffectRequest("Dimm");
});

// RAINBOW BUTTON
// const SLIDER_ACTIVE_TIME = 5 * 1000;
rainbow_button.addEventListener("click", event => {
    // // clear slider timeout if exists
    // clearTimeout(slider_container.slider_timeout_id);

    // show speed slider and hide again after 10 seconds
    slider_container.classList.add("visible");
    // slider_container.slider_timeout_id = setTimeout(() => {slider_container.classList.remove("visible")}, SLIDER_ACTIVE_TIME);

    sendEffectRequest("Rainbow", slider.value);
});

// SPEED SLIDER
slider.addEventListener("input", event => {
    // // clear slider timeout if exists and set it again
    // clearTimeout(slider_container.slider_timeout_id);
    // slider_container.slider_timeout_id = setTimeout(() => {slider_container.classList.remove("visible")}, SLIDER_ACTIVE_TIME);

    // display slider value
    const value = event.target.value;
    output.innerHTML = value;

    sendEffectRequest("Rainbow", value)
});

// COLOR PICKER
// attach button to color input
color_button.addEventListener("click", event => {
    hideSpeedSlider();
    // trigger color input element
    color.dispatchEvent(new MouseEvent("click"));
})
// trigger input event on click
color.addEventListener("click", () => {
    color.dispatchEvent(new InputEvent("input"));
})
color.addEventListener("input", event => {
    // set Button to picked color
    color_button.style.borderColor = event.target.value;

    // remove leading '#'
    const color = event.target.value.slice(1);
    sendEffectRequest("Color", color);
});


// change color picker buttons color on mousehover
// dynamic :hover
color_button.addEventListener("mouseover", () => {
    // save old background color
    color_button.oldColor = color_button.style.backgroundColor;
    color_button.style.backgroundColor = color.value;
});
color_button.addEventListener("mouseout", () => {
    // reset to old background color
    color_button.style.backgroundColor = color_button.oldColor;
});
//#endregion


// -- TAB BUTTONS --
const tab_control_button = document.getElementById("tab-control");
const tab_schedule_button = document.getElementById("tab-schedule");
function openTab(event) {
    // disable tabs by default
    const tabs = document.querySelectorAll(".tab-content");
    tabs.forEach(tab => {
        tab.classList.remove("visible");
    });
    if(event.target.id === "tab-control") {
        document.querySelector(".app").classList.add("visible");
    }
    else {
        document.querySelector(".alarm").classList.add("visible");
        getAlarmList();
    }
}
tab_control_button.onclick = openTab;
tab_schedule_button.onclick = openTab;

// -- ALARM SECTION --
function getAlarmList() {
    const destination = "/alarm";
    const request = new XMLHttpRequest();
    request.onreadystatechange = alarmListRequestHandler;
    request.open("GET", destination);
    request.send();
}

function clearAlarmList() {
    const childs = document.querySelector(".alarm-list-container").children;
    Array.from(childs).forEach(child => {
        child.remove();
    });
}
function alarmListRequestHandler() {
    const alarm_list_container = document.querySelector(".alarm-list-container");
    if(this.readyState === 4 && this.status === 200) {
        // first remove all the existing alarms
        clearAlarmList();

        // parse alarm list into array
        const alarm_list = JSON.parse(this.responseText);

        // sort by comparing time with custom sort function
        // see https://www.w3schools.com/js/js_array_sort.asp
        alarm_list.sort((a,b) => {
            const hourA = Number(a.time.slice(0,2));
            const hourB = Number(b.time.slice(0,2));
            const minuteA = Number(a.time.slice(3));
            const minuteB = Number(b.time.slice(3));
            if(hourA < hourB) return -1;
            if(hourA > hourB) return +1;
            // same hour
            return (minuteA <= minuteB ? -1 : + 1);
        })

        // create new alarm card and initialize it
        alarm_list.forEach(alarm => {
            const alarm_card = document.createElement("alarm-template");
            alarm_card.initializeAlarm(alarm);
            alarm_list_container.appendChild(alarm_card);
        });
    }
}
