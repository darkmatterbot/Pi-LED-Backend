const { DataTypes, Model } = require("sequelize");

class Alarm extends Model {};

module.exports = sequelize => {
    Alarm.init({
        // Model attributes are defined here
        name: {
            type: DataTypes.STRING,
        },
        type: {
            type: DataTypes.INTEGER
        },
        time: {
            type: DataTypes.STRING,
            allowNull: false
        },
        days: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        effect: {
            type: DataTypes.STRING,
            allowNull: false
        },
        effectparam: {
            type: DataTypes.STRING,
            allowNull: false
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        sequelize: sequelize    // db connection instance
    });
};