#!/usr/bin/node

"use strict";

const Log = {
	reset: "\x1b[0m",
	bright: "\x1b[1m",
	dim: "\x1b[2m",
	underscore: "\x1b[4m",
	blink: "\x1b[5m",
	reverse: "\x1b[7m",
	hidden: "\x1b[8m",
	// Foreground (text) colors
	fg: {
		black: "\x1b[30m",
		red: "\x1b[31m",
		green: "\x1b[32m",
		yellow: "\x1b[33m",
		blue: "\x1b[34m",
		magenta: "\x1b[35m",
		cyan: "\x1b[36m",
		white: "\x1b[37m",
		crimson: "\x1b[38m"
	},
	// Background colors
	bg: {
		black: "\x1b[40m",
		red: "\x1b[41m",
		green: "\x1b[42m",
		yellow: "\x1b[43m",
		blue: "\x1b[44m",
		magenta: "\x1b[45m",
		cyan: "\x1b[46m",
		white: "\x1b[47m",
		crimson: "\x1b[48m"
	}
};

const express = require("express");
const database = require("./config/database");
const controller = require("./controllers");
const router = require("./routers");

const app = express();
app.disable("x-powered-by");
app.use(express.json());
app.use(express.static(__dirname + "/public"));

// PORT SETUP
let HTTP_PORT = Number(process.argv[2])
let LED_SERVER_PORT= Number(process.argv[3])
if (isNaN(HTTP_PORT) | isNaN(LED_SERVER_PORT) | HTTP_PORT < 1024 | HTTP_PORT > 65535 | LED_SERVER_PORT < 1024 | LED_SERVER_PORT > 65535) {
  HTTP_PORT = 33333;
  LED_SERVER_PORT = 55555;
}
console.log(`using ports ${HTTP_PORT}(HTTP) and ${LED_SERVER_PORT}(LED)`);

// connect to led server
controller.LED.init(LED_SERVER_PORT);

async function addDefaultAlarms() {
    const DEFAULT_ALARMS = [
        {name:"HomeAway",type:1,time:"10:30",days:127,effect:"Rainbow",effectparam:"100",enabled: 1},
        {name:"Sunrise Weekend",type:2,time:"08:45",days:96,effect:"Color",effectparam:"ffff00",enabled: 0},
        {name:"Sunrise Weekdays",type:2,time:"07:45",days:31,effect:"Rainbow",effectparam:"100",enabled: 1},
        {name:"Sunset",type:3,time:"22:30",days:77,effect:"Off",effectparam:"",enabled: 1},
        {name:"Timer1",type:4,time:"12:34",days:64,effect:"Rainbow",effectparam:"50",enabled: 1},
        {name:"Party 11:59",type:5,time:"11:59",days:127,effect:"Bouncing Balls",effectparam:"10",enabled: 0}
    ];

    await database.models.Alarm.bulkCreate(DEFAULT_ALARMS);
    const alarm_list = await database.models.Alarm.findAll();
    for (const alarm of alarm_list) {
        if (alarm.enabled) {
            controller.cron.createJob(alarm);
        }
    }
}
// addDefaultAlarms();


//  -- Helper Functions --
function log(req, res, next) {
    const col = Log.fg.green, rst = Log.reset;
	console.log(col + req.method, req.url + rst, "from", col + req.ip + rst);
	next();
}
app.use(log);



// -- ENDPOINTS --
app.get("/greeting/", (req, res) => {
    res.sendStatus(200);
});
// requests for Event streams
app.get("/connection-status", controller.connection_status.stream);

// -- EFFECT ENDPOINT --
app.use("/effect", router.effect);
app.get("/previous/", controller.effect.previous);
// -- ALARM ENDPOINT --
app.use("/alarm", router.alarm);


// ignore favicon
app.get("/favicon.ico", (req, res) => {
  res.sendStatus(204);
});



// check database connection and start server
(async () => {
    try {
        await database.authenticate();
        console.log("Connection to Database has been established successfully");
        app.listen(HTTP_PORT, "0.0.0.0", () => {
            console.log("http server listening on port " + HTTP_PORT);
        });
    } catch (error) {
        console.error(error);
    }
})();

// -- SIGNAL HANDLER for shutting down gracefully --
async function exitHandler(signal) {
    console.log(`received ${signal}, stopping server`);
    // close connection to led server and database
    try {
        controller.LED.sock.end();
        await database.close();
        console.log("server stopped\n\n");
        process.exit(0);
    
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
}
process.on("SIGTERM", exitHandler);
process.on("SIGINT", exitHandler);
