#!/bin/bash -e
NC='\e[0m'
BOLD="\e[1m"
UNDERL="\e[4m"
RED='\e[0;31m'
GREEN='\e[32m'
BLUE="\e[34m"
YELLOW="\e[93m"
TICK="${NC}[${GREEN}✓${NC}]"
CROSS="[${RED}✗${NC}]"
echo -e "$BOLD\nHi $UNDERL$1$NC$BOLD,"
echo -e "your current project state will be deployed on your$RED raspberry pie$NC$BOLD for a stable version of this great$RED L$GREEN E$YELLOW D$NC$BOLD - software$NC$BOLD$GREEN ;)$NC$TICK$TICK$TICK$NC"
