# Endpoints

```json
GET /greeting
  for debugging purposes
```

```json
GET /previous
  get previous color in HEX [000000-ffffff]
```

```json
POST /effect where POST body is a json object:
      {
        "effect":       <String>,
        "effectparam":  <String>
      }

      "effect" values:
        Color
        Rainbow
        Bouncing Balls
        Sunrise
        Dimm
        Off

      "effectparam" values (optional):
        either a HEX coded color value [000000-ffffff]
        or an integer speed value [1-100]

```

```json
GET /alarm returns an array of json (alarm)objects:
      [
        {
        "id":           <integer>,
        "name":         <String>,
        "type":         <integer>,
        "time":         <String>,
        "days":         <integer>,
        "effect":       <String>,
        "effectparam":  <String>,
        "enabled":      <integer>
        },
        {
          ...
        },
        {
          ...
        }
      ]

      "id"          a number
      "name"        values: any name
      "type"        identifier for the alarm-type --> Home & Away, Sunrise, Sunset, Timer, Custom 
      "time"        is 24-hour format
      "days"        8bit encoded -> LSB = Monday, All days = 127 = 0b01111111 = 2^(7)-1
      "effect"      see /effect endpoint above
      "effectparam" see /effect endpoint above
      "enabled"     1 or 0 
```

```json
PUT /alarm/:id where PUT body is a json (alarm)object:
      {
        "name":         <String>,
        "type":         <integer>,
        "time":         <String>,
        "days":         <integer>,
        "effect":       <String>,
        "effectparam":  <String>,
        "enabled":      <integer>
      }

```

```json
DELETE /alarm/:id where id is alarm to be deleted:
  Deletes the alarm with given it, if it exists

```
