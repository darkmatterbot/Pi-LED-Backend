#!/usr/bin/python3
# coding=utf-8

# BouncingBalls2014 is a program that lets you animate an LED strip
# to look like a group of bouncing balls

# Daniel Wilson, 2014

# With BIG thanks to the FastLED community!
# ported to python by Benedikt Maus

import math
import time

import Adafruit_GPIO.SPI as SPI
import led_scripts.lib.WS2801 as WS2801

SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))


SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

class Balls:
    GRAVITY =   -9.81              # Downward (negative) acceleration of gravity in m/s^2
    h0 =        1                  # Starting height, in meters, of the ball (strip length)
    vImpact0 = math.sqrt( -2 * GRAVITY * h0 );      # Impact velocity of the ball when it hits the ground if "dropped" from the top of the strip
    BALL_COLORS = [
                    [255,0,0], [0,255,0], [0,0,255],
                    [255,255,0], [0,255,255], [255,0,255],
                    [255,255,255]
                    ]
    BC_LEN = len(BALL_COLORS)

    def init_balls(self,n):
        self.NUM_BALLS = n
        self.h = [0] * self.NUM_BALLS                             # An array of heights
        self.vImpact = [0] * self.NUM_BALLS                       # As time goes on the impact velocity will change, so make an array to store those values
        self.tCycle  = [0] * self.NUM_BALLS                       # The time since the last time the ball struck the ground
        self.pos     = [0] * self.NUM_BALLS                       # The integer position of the dot on the strip (LED index)
        self.tLast   = [0] * self.NUM_BALLS                       # The clock time of the last ground strike
        self.COR     = [0] * self.NUM_BALLS                       # Coefficient of Restitution (bounce damping)

        for i in range(self.NUM_BALLS):
            self.tLast[i] = time.time() / (1000)
            self.h[i] = self.h0
            self.pos[i] = 0;                             # Balls start on the ground
            self.vImpact[i] = self.vImpact0;                  # And "pop" up at vImpact0
            self.tCycle[i] = 0
            self.COR[i] = 0.90 - (float(i) / self.NUM_BALLS ** 2)

    def bounce(self, piss):
        PIXEL.clear()
        self.init_balls(3)
        while True:
            for i in range(self.NUM_BALLS):
                self.tCycle[i] =  (time.time_ns() / (1000 * 1000)) - self.tLast[i]     # Calculate the time since the last time the ball was on the ground

                # A little kinematics equation calculates positon as a function of time, acceleration (gravity) and intial velocity
                self.h[i] = 0.5 * self.GRAVITY * pow((self.tCycle[i] / 1000), 2.0) + (self.vImpact[i] * (self.tCycle[i] / 1000))

                if self.h[i] < 0:
                    self.h[i] = 0;                            # If the ball crossed the threshold of the "ground," put it back on the ground
                    self.vImpact[i] = self.COR[i] * self.vImpact[i] ;   # and recalculate its new upward velocity as it's old velocity * COR
                    self.tLast[i] = time.time_ns() / (1000 * 1000)

                    if self.vImpact[i] < 0.01:
                        self.vImpact[i] = self.vImpact0;          # If the ball is barely moving, "pop" it back up at vImpact0
                self.pos[i] = round( self.h[i] * (PIXEL_COUNT - 1) / self.h0);       # Map "h" to a "pos" integer index position on the LED strip
                # print(pos[i])

            #Choose color of LEDs, then the "pos" LED on
            for i in range(self.NUM_BALLS):
                PIXEL.set_pixel_rgb(self.pos[i],self.BALL_COLORS[i%self.BC_LEN][0], self.BALL_COLORS[i%self.BC_LEN][1], self.BALL_COLORS[i%self.BC_LEN][2])

            PIXEL.show()

            #Then off for the next loop around
            for i in range(self.NUM_BALLS):
                PIXEL.set_pixel_rgb(self.pos[i], 0,0,0)


if __name__ == "__main__":
    Balls().bounce("PISSE")
