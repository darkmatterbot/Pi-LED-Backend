#!/usr/bin/python3
# coding=utf-8

import queue, math, random, time
import Adafruit_GPIO.SPI as SPI
import led_scripts.lib.WS2801 as WS2801

SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

PIXEL.clear()
PIXEL.show()
