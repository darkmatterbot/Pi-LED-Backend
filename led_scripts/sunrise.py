#!/usr/bin/python3
# coding=utf-8

import queue, math, random, time
import Adafruit_GPIO.SPI as SPI
import lib.WS2801 as WS2801
import sys

SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

# 30 min --> 30*60 = 1.800
# 1.800 / 255 = 7 secs Delay or Sleeptime between brightness increase
def sunrise():
    print("Sunrise started")
    PIXEL.clear()
    SLEEP = 0.001
    DELAY = (60*float(sys.argv[1]))/255
    print("Duration: %i, Delay: %.2f"%(float(sys.argv[1]), DELAY))

    # helligkeit von 0 auf 255
    for j in range(256):  # one cycle of all 256 colors in the wheel
        for i in range(PIXEL.count()):
            r = j
            g = int(j/2.5)
            b = int(j/6)
            PIXEL.set_pixel_rgb(i, r,b,g)
        PIXEL.show()

        t0 = 0
        while t0 < DELAY:
            t0 = t0+SLEEP
            time.sleep(SLEEP)

        print("R: %i, B: %i, G: %i"%(r,b,g))
    print("Moin")

if __name__ == "__main__":
    PIXEL.clear()
    sunrise()
