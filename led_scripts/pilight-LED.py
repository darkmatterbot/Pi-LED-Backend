#!/usr/bin/python3
# coding=utf-8

import logging
import logging.config
import queue
import signal
import socket
import sys
import threading
import time

import lib.LedFunctions as LED
import lib.Message as MSG

# get log directory
import os
LOG_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "log")
config_file = os.path.join(LOG_DIR, "logging.conf")
log_file    = os.path.join(LOG_DIR, "led.log")

logging.config.fileConfig(config_file, defaults={"logfilename": log_file})
logger = logging.getLogger("LedServer")


class EffectThread:
    def __init__(self):
        self.thread = None
        self.kill_switch = None
        self.queue = None
        self.last_param = None

    def start(self, effect, effectparam):
        self.queue = queue.Queue()
        self.kill_switch = threading.Event()
        self.queue.put(effectparam)
        self.last_param = effectparam
        if effect == "Rainbow":
            logger.debug("starting rainbow")
            self.thread = threading.Thread(target=LED.rainbow, args=(self.kill_switch, self.queue), daemon=True)
        elif effect == "Bouncing Balls":
            self.thread = threading.Thread(target=LED.Balls().bounce, args=(self.kill_switch, self.queue), daemon=True)
            logger.debug("starting bounce")
        elif effect == "Sunrise":
            self.thread = threading.Thread(target=LED.sunrise, args=(self.kill_switch, effectparam), daemon=True)
            logger.debug("starting Sunrise")
        self.thread.start()

    def stop(self):
        # if rainbow runs, we need to stop it
        if self.thread and self.thread.is_alive():
            logger.debug("stopping effect...")
            #self.queue.put("stop")
            self.kill_switch.set()
            # wait for thread to finish
            self.thread.join()
            self.thread = None
            logger.debug("stopped effect!")

    def update(self, effectparam):
        if effectparam != self.last_param:
            logger.debug("updating paramater")
            self.last_param = effectparam
            self.queue.put(effectparam)

    def is_running(self):
        return True if self.thread != None else False



# signal handler for SIGINT and SIGTERM
def exitHandler(signum, frame):
    logger.debug(f"received SIGNAL {signum}, stopping script")
    # sock.shutdown(socket.SHUT_RDWR)
    # client.close()
    sock.close()
    logger.debug("socket closed")
    LED.setColor("0")
    logger.debug("script stopped\n\n\n\n")
    exit(0)
signal.signal(signal.SIGINT, exitHandler)
signal.signal(signal.SIGTERM, exitHandler)



# check if port was provided and if it is valid
#otherwise use standard port
BIND_PORT = 55555
try:
    PROVIDED_PORT = int(sys.argv[1])
    if PROVIDED_PORT < 1024 or PROVIDED_PORT > 65535:
        raise ValueError(f"port must be in the range [1024 - 65535]")
    else:
        BIND_PORT = PROVIDED_PORT
        logger.debug(f"using port {BIND_PORT}")
except ValueError:
    logger.error(f"provided port {PROVIDED_PORT} is not valid")
    logger.debug(f"using fallback port {BIND_PORT}")
except IndexError:
    logger.error("no port was provided")
    logger.debug(f"using fallback port {BIND_PORT}")

# socket setup
TIMEOUT = 10
sock = socket.socket()
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
try:
    logger.debug("Trying to bind socket")
    sock.bind(("127.0.0.1", BIND_PORT))
    sock.listen()
except OSError:
    logger.error(f"Could not bind socket on Port {BIND_PORT}\nexiting...\n\n\n\n")
    exit(1)
logger.debug(f"socket is listening on {BIND_PORT}")



def main():
    # object representing a thread and a queue to communicate with thread
    EffectRunner = EffectThread()
    LAST_EFFECT = None

    # connection loop
    while True:
        logger.debug(f"waiting for client")
        # accept a new client (the node server)
        client, address = sock.accept()
        logger.debug(f"client from {address} connected")

        # message loop
        while True:
            # get data from client
            data = MSG.getMsg(client)
            if not data:
                client.close()
                logger.debug("client disconnected")
                break

            effect = data["effect"]
            effectparam = data["effectparam"]

            # stop any running effect
            if effect != LAST_EFFECT:
                EffectRunner.stop()
                LAST_EFFECT = effect

            # parse data
            if effect == "Off":
                # set the LED
                color = "000000"
                LED.setColor(color)

            elif effect == "Dimm":
                # set the LED
                color = "ff1100"
                LED.setColor(color)

            elif effect == "Color":
                # set the LED
                color = effectparam
                LED.setColor(color)

            elif effect == "Bouncing Balls":
                num_balls = int(effectparam)
                # if bouncing effect runs, no need to stop it
                if EffectRunner.is_running():
                    EffectRunner.update(num_balls)
                    continue
                else:
                    EffectRunner.start(effect, num_balls)

            elif effect == "Rainbow":
                speed = int(effectparam)
                # if rainbow runs, no need to stop it
                if EffectRunner.is_running():
                    EffectRunner.update(speed)
                    continue

                #create a new thread object and run it
                else:
                    EffectRunner.start(effect, speed)
            elif effect == "Sunrise":
                duration = int(effectparam)
                # if rainbow runs, no need to stop it
                if EffectRunner.is_running():
                    EffectRunner.update(duration)
                    continue

                #create a new thread object and run it
                else:
                    EffectRunner.start(effect, duration)

if __name__ == "__main__":
    main()