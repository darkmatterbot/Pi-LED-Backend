#!/usr/bin/python3
# coding=utf-8

import queue, math, random, time
import Adafruit_GPIO.SPI as SPI
import led_scripts.lib.WS2801 as WS2801

SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

def Fire(cooling, sparking, delay):
    heat = [0]*PIXEL_COUNT

    while True:
        # Step 1.  Cool down every cell a little
        for i in range(PIXEL_COUNT):
            cooldown = random.randint(1, int(((cooling*10)/PIXEL_COUNT)+2))
            if cooldown > heat[i]:
                heat[i]=0
            else:
                heat[i]=heat[i]-cooldown

        # Step 2.  Heat from each cell drifts 'up' and diffuses a little
        k = PIXEL_COUNT-1
        while k > 1:
            heat[k] = (heat[k-1] + heat[k-2]+heat[k-2])/3
            k -= 1

        # Step 3.  Randomly ignite new 'sparks' near the bottom
        if random.randint(0, 255) < sparking:
            y = random.randint(0,7)
            heat[y] = heat[y] + random.randint(160, 255)

        # Step 4.  Convert heat to LED colors
        for i in range(PIXEL_COUNT):
            setPixelHeatColor(i, heat[i])
        PIXEL.show()
        time.sleep(delay/1000)
def setPixelHeatColor(pixel, temperature):
    t192 = random.randint(0,int((temperature/255)*191)%191)
    heatramp = t192 & 0x3F # 0..63
    heatramp <<= 2 # scale up to 0..252
    print(t192)
    if t192 > 0x80:
        PIXEL.set_pixel_rgb(pixel, 255, 255, heatramp)
    elif t192 > 0x40:
        PIXEL.set_pixel_rgb(pixel, 255, heatramp, 0)
    else:
        PIXEL.set_pixel_rgb(pixel, heatramp, 0,0)

if __name__ == "__main__":
    Fire(50,200,50)
