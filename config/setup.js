const createJob = require("../controllers/cron").createJob;

module.exports = async database => {
    console.log("enabling alarms");
    const alarms = await database.models.Alarm.findAll();
    for (const alarm of alarms) {
        if (alarm.enabled) {
            createJob(alarm);
        }
    }
}