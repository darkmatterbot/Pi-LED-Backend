const { Sequelize } = require("sequelize");
const setup = require("./setup");

const db_name = "app.db";

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: __dirname + "/" + db_name,
    logging: false
});

const modelDefiners = [
    require("../models/Alarm")
];

// We define all models according to their files.
for (const modelDefiner of modelDefiners) {
	modelDefiner(sequelize);
}

// create table if not exists
(async () => {
    await sequelize.sync();
    await setup(sequelize);
})();



module.exports = sequelize;