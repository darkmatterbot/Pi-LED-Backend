const LED = require("./led");

// requests for Event streams
function stream(req, res) {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  // add client to internal array for status updates
  LED.addForStatusUpdate(res);

  // push status to client
  LED.emitStatus();

  // remove client from internal array for status updates
  // when he closes connection
  res.on("close", () => {
    console.log("client closed connection");
    LED.removeFromStatusUpdate(res);
  });
}

module.exports = {
    stream
}