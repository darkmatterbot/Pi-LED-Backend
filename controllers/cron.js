const cron = require('node-cron');
const LED = require("./led");

const alarmList = [];

function generateCronTime(alarm) {
    // 18:40 --> 18
    const cron_hour =   alarm.time.slice(0,2)
    // 18:40 --> 40
    const cron_minute = alarm.time.slice(3,5);

    // days value is 8bit int, each bit representing a weekday, 5 --> 00000101 --> Wednesday, Monday
    // Monday is LSB
    const days_value = alarm.days;
    let cron_days = "";
    if(days_value === 0) {
        // 0 means any day
        cron_days = "*";
    }
    else {
        for(let i = 0; i < 7; i++) {
            // shift to the right and only look at LSB
            if((days_value >> i) & 1) {
                // add day to cron expression
                cron_days += `${i+1},`
            }
        }
        // remove the last comma, 2,4,7, --> 2,4,7
        cron_days = cron_days.slice(0,-1);
    }


    // construct final cron expression
    const cron_time = `${cron_minute} ${cron_hour} * * ${cron_days}`;
    

    if(cron.validate(cron_time) === false) {
        // console.log(`${cron_time} is not valid!`);
        return false;
    }
    // console.log(`${cron_time} is valid!`);
    return cron_time;
}

function createJob(alarm) {
    let task = undefined;
    const cron_time = generateCronTime(alarm);
    // see if it is an alarm that should be repeated
    if(alarm.days !== 0) {
        // schedule the task
        task = cron.schedule(cron_time, () => {
            console.log(`executing '${alarm.name}' Effect: ${alarm.effect}`);
            LED.sendMsg(alarm.effect, alarm.effectparam)
        });
    }
    else {
        // schedule the task
        // but disable it after it ran
        task = cron.schedule(cron_time, async () => {
            console.log(`executing '${alarm.name}' Effect: ${alarm.effect}`);
            LED.sendMsg(alarm.effect, alarm.effectparam);
            task.stop();
            alarm.enabled = false;
            await alarm.save();
        });
    }

    console.log(`scheduled task '${alarm.name}' Effect: ${alarm.effect}`);
    
    // attach task to job and push to internal list
    const job = {};
    job.id = alarm.id
    job.task = task;
    alarmList.push(job);
}

function destroyJob(alarm) {
    // find alarm
    const index = alarmList.findIndex(el => el.id === alarm.id);
    if (index === -1) {
        return console.log(`WARNING: cannot find alarm ${alarm.id} in ram`);
    }
    alarmList[index].task.stop();
    alarmList.splice(index, 1);
    return console.log(`task ${alarm.name} stopped and destroyed`);
}

module.exports = {
    generateCronTime,
    createJob,
    destroyJob
}