module.exports = {
    alarm: require("./alarm"),
    connection_status: require("./connectionStatus"),
    cron: require("./cron"),
    effect: require("./effect"),
    LED: require("./led")
}