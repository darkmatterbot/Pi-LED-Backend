'use strict'

const database = require("../config/database");
const { Alarm } = database.models;
const { generateCronTime, createJob, destroyJob } = require("./cron");

const createAlarm = async (req, res) => {
    console.group();
    console.log("createAlarm()", req.body);
    try {
        const cron_time = generateCronTime(req.body);
        if (cron_time === false) {
            return res.status(400).send("Invalid time format");
        }

        const alarm = await Alarm.create({
            name: req.body.name,
            type: req.body.type,
            time: req.body.time,
            days: req.body.days,
            effect: req.body.effect,
            effectparam: req.body.effectparam,
            enabled: req.body.enabled
        });

        if (alarm.enabled) {
            createJob(alarm);
            console.log("created alarm", alarm);
        }

        res.sendStatus(200);
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
    console.groupEnd();
};


const getAlarms = async (req, res) => {
    console.group();
    try {
        const alarms = await Alarm.findAll({
            attributes: ["id", "name", "type", "time", "days", "effect", "effectparam", "enabled"]
        });
        res.status(200).json(alarms);
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
    console.groupEnd();
};

const getAlarmById = async (req, res) => {
    console.group();
    try {
        const id = Number(req.params.id);
        const alarm = await Alarm.findByPk(id, {
            attributes: ["id", "name", "type", "time", "days", "effect", "effectparam", "enabled"]
        });
        if (alarm === null) {
            return res.sendStatus(404);
        }

        res.status(200).json(alarm);
    } catch (err) {
        console.log(error);
        res.sendStatus(500);
    }
    console.groupEnd();
};

const updateAlarm = async (req, res) => {
    console.group();
    console.log("updateAlarm()", req.body);
    try {
        const id = Number(req.params.id);
        const alarm = await Alarm.findByPk(id);
        if (alarm === null) {
            return res.sendStatus(404);
        }

        // check time
        const cron_time = generateCronTime(req.body);
        if (cron_time === false) {
            return res.status(400).send("Invalid fime format");
        }

        // if the alarm was enabled, we need to remove it so we can start it again with new parameters
        if (alarm.enabled) {
            destroyJob(alarm);
        }

        for (const key in req.body) {
            // we are not interested in inherited properties
            if (req.body.hasOwnProperty(key)) {
                if (req.body[key] !== alarm[key]) {
                    console.log(`changed ${key}: ${alarm[key]} ==> ${req.body[key]}`);
                    alarm[key] = req.body[key];
                }
            }
        }
        await alarm.save();

        if (alarm.enabled) {
            createJob(alarm);
        }

        res.sendStatus(200)
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
    console.groupEnd();
};

const deleteAlarm = async (req, res) => {
    console.group();
    try {
        const id = req.params.id;
        const alarm = await Alarm.findByPk(id);
        if (alarm === null) {
            return res.sendStatus("404");
        }

        if (alarm.enabled) {
            destroyJob(alarm);
        }

        await alarm.destroy();
        res.sendStatus(200);
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
    console.groupEnd();
};

module.exports = {
    createAlarm,
    getAlarms,
    getAlarmById,
    updateAlarm,
    deleteAlarm
}
