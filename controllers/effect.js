const LED = require("./led");

const PREVIOUS = {
    effect: null,
    param: null
};

// app.get('/previous/', (req, res) => {
const previous = (req, res) => {
    console.group();
    LED.sendMsg(LAST_REQUEST.effect, LAST_REQUEST.param);
    res.status(200).json({"url":LAST_REQUEST.effect, "param":LAST_REQUEST.param});
    console.groupEnd();
}

const update = (req, res) => {
    console.group();
    console.log(req.body);

    // ensure parameters are correct
    const effect = req.body.effect;
    const effectparam = req.body.effectparam;
    if(typeof(effect) === 'undefined' || typeof(effectparam) === 'undefined') {
        console.log("bad request");
        return res.sendStatus(400);
    }

    // save request
    if(effect !== "Off") {
        PREVIOUS.effect = effect;
        PREVIOUS.param = effectparam;
    }
    LED.sendMsg(effect, effectparam);
    res.sendStatus(200);
    console.groupEnd();
}

module.exports = {
    previous,
    update
}