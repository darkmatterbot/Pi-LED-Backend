# Systemctl services
#### How to get it up and running

Start both services with
```
sudo systemctl start led-backend.service
```
or
```
sudo systemctl start node-backend.service
```
They each require each other, so whenever anyone of those is started, it starts the other one. Same applies if one out of the two dies.


## 1. Create a .service file and configure it
e.g. `test.service`
```
[Unit]
Description="LED Backend"
Requires=i_am_bound_to_this.service
After=network.target

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
PIDFile=/run/led-backend.pid
ExecStart=/path/to/script.whatever (args)
User=John
Restart=on-failure

```

## 2. Copy the file to `/etc/systemd/system/`
```
sudo cp test.service /etc/systemd/system/
```